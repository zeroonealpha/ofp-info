<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class flights extends Model
{
    protected $table = "flights_done_general";

    protected $connection = "weststar";
}
