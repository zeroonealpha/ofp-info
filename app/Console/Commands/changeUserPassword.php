<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class changeUserPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'password:change {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the Client Login password for a user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get the user by {username}
        $user = User::where('name', $this->argument('username'))->get()->first();

        $newPass = $this->ask('New Password?');

        $user->password = bcrypt($newPass);

        if($user->save())
        {
            $this->info('Password Changed Successfully.');
        } else {
            $this->info('Password change error.');
        }
    }
}
