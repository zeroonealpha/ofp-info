<?php

namespace App\Http\Controllers;

use App\Mail\ContactEnquiry;
use App\Mail\LicenseEnquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function licensing(Request $request)
    {
        // Validate
        $this->validate($request, [
           'name' => 'required',
            'company' => 'required',
            'email' => 'email|required',
            'telephone' => 'required',
            'city' => 'required',
            'country' => 'required',
         ]);

        // Check the robot
        $numberOfBlades = $request->input("robot");

        if ($numberOfBlades != 5) {
            return view('contact')->withMessage("Sorry, are you sure you are not a robot?");
        }

        // Send email
        Mail::to(env('APP_EMAIL'))->send(new LicenseEnquiry($request->all()));

        // Say Thanks
        return view('thanks')->withMessage('Thank you for your licensing enquiry. We will be in touch as soon as possible.');
    }

    public function contact(Request $request)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required',
            'company' => 'required',
            'email' => 'email|required',
            'telephone' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);
        // Check the robot
        $numberOfBlades = $request->input("robot");

        if ($numberOfBlades != 5) {
            return view('licensing')->withMessage("Sorry, are you sure you are not a robot?");
        }

        // Send email
        Mail::to(env('APP_EMAIL'))->send(new ContactEnquiry($request->all()));

        // Say Thanks
        return view('thanks')->withMessage('Thank you for contacting us. We will be in touch as soon as possible');
    }
}
