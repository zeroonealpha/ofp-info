<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function download($client)
    {
        if($client != auth()->user()->operator_id) return redirect('/');

        return response()->download(storage_path('app/public/' . $client .  '/offshore_flightplan.msi'));
    }

    public function downloadPerformanceApp($client)
    {
        if($client != auth()->user()->operator_id) return redirect('/');

        if($client != 'bsp') return redirect('/');

        return response()->download(storage_path('app/public/' . $client .  '/OFP_Performance_Setup.msi'));
    }
}
