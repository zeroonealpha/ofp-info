<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsletterSubscribers;

class NewsletterController extends Controller
{
    public function index()
    {
        return view('newsletter');
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
           'email' => 'email|required'
        ]);


        if($request->input('subscribed'))
        {
            // Insert email
            NewsletterSubscribers::create($request->all());

            return view('subscribed')->withMessage('Thanks for subscribing!');
        }

        // Change entry to 'unsubscribed'
        $subscribers = NewsletterSubscribers::where('email', $request->input('email'))->get();

        if($subscribers->count() > 0) {
            $subscriber = $subscribers->first();
            $subscriber->subscribed = 0;
            $subscriber->save();
            return view('subscribed')->withMessage('You have been removed from the newsletter mailing list :-(');
        }

        return view('subscribed')->withMessage('Sorry, that email address cannot be found on the mailing list.');
    }
}
