<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth;

class ClientAreaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($client)
    {
        // Check client is authorized to view
        if($client != auth()->user()->name) return redirect('/')->withMessage('You are NOT AUTHORIZED to access that area.');

        return view('client')->withClient($client);
    }

}
