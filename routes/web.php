<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/licensing', function() { return view('licensing'); });
Route::get('/contact', function() { return view('contact'); });
Route::get('/newsletter', 'NewsletterController@index');
Route::post('/newsletter/subscribe', 'NewsletterController@subscribe');

Route::post('/licensing/enquiry', 'ContactController@licensing');
Route::post('/contact/message', 'ContactController@contact');

Route::get('/client-area/{client}', 'ClientAreaController@show');
Route::get('/logout', 'ClientAreaController@logout');

Route::get('/download/manual', function() {
    return response()->download(public_path('/assets/downloads/offshore_flightplan_manual.pdf'));
});

Route::get('/download/manual/performance', function() {
    return response()->download(public_path('/assets/downloads/OFP_Performance_User_Manual.pdf'));
});


Route::get('/download/vc', function() {
    return response()->download(storage_path('/app/public/vcredist_x86.exe'));
});

Route::get('/download/{client}/ofp', 'DownloadController@download');
Route::get('/download/{client}/performance', 'DownloadController@downloadPerformanceApp');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
