<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="./favicon.png">

    <title>

        OffshoreFlightPlan

    </title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/application-minimal.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <style>
        /* Sticky footer styles
        -------------------------------------------------- */
        html {
            position: relative;
            min-height: 100%;
        }
        body {
            margin-bottom: 60px; /* Margin bottom by footer height */
        }
        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 300px; /* Set the fixed height of the footer here */
            line-height: 60px; /* Vertically center the text there */
            background-color: #f5f5f5;
        }

        .footer-link {
            color: #eee;
        }

        .footer-link:hover {
            color: #629ED6;
        }


    </style>

</head>
<body>

<div class="container pt-4">
    @include('partials.navbar')
</div>
<div class="container">
    <h2 class="mt-0">
        <strong class="text-ribbon text-ribbon-info">
            <span><span class="icon icon-arrow-right"></span> Newsletter</span>
        </strong>
    </h2>
    <p class="mt-4">
      <span class="text-ribbon text-ribbon-info text-white">
        <span>Subscribe or Unsubscribe below</span>
      </span>
    </p>
    <br>
    @include('partials.errors')
    <form class="form-inline" method="POST" action="/newsletter/subscribe">
        {{ csrf_field() }}
        <div class="form-row align-items-center">
            <div class="col-auto">
                <label class="sr-only" for="inlineFormInputGroup">Email</label>
                <div class="input-group mb-2">
                    <div class="input-group-addon">
                        <div class="input-group-text">@</div>
                    </div>
                    <input type="email" class="form-control" id="inlineFormInputGroup" placeholder="mail@domain.com" name="email">
                </div>
            </div>
            <div class="col-auto">
                <select class="custom-select mb-2" name="subscribed">
                    <option selected value="1">Subscribe</option>
                    <option value="0">UnSubscribe</option>
                </select>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </div>
        </div>
    </form>
</div>

<footer class="footer">
    @include('partials.footer')
</footer>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>










