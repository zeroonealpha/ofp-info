<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="./favicon.png">

    <title>OffshoreFlightPlan</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/application-minimal.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <style>
        /* note: this is a hack for ios iframe for bootstrap themes shopify page */
        /* this chunk of css is not part of the toolkit :) */
        /* …curses ios, etc… */
        @media (max-width: 768px) and (-webkit-min-device-pixel-ratio: 2) {
            body {
                width: 1px;
                min-width: 100%;
                *width: 100%;
            }
            #stage {
                height: 1px;
                overflow: auto;
                min-height: 100vh;
                -webkit-overflow-scrolling: touch;
            }
        }

        .footer-link {
            color: #eee;
        }

        .footer-link:hover {
            color: #629ED6;
        }


    </style>
</head>
<body>

<div class="container pt-4">
    @include('partials.client-navbar')
</div>
<div class="container">
    <div class="block text-center">
        <h3 class="block-title"><span class="icon icon-lock"></span> Secure Client Download Area</h3>
        <hr>
        <img class="img-fluid align-content-center w-25" src="/assets/img/{{ $client }}.png">
        <h4 class="text-muted">.</h4>
        <div class="row justify-content-md-center">
            <div class="col-md-3">
                <div class="card border-dark">
                    <div class="card-header">
                        <h5 class="card-title">Offshore FlightPlan</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ Storage::get('public/' . $client . '/ofp.txt') }}</p>
                        <a href="/download/{{ $client }}/ofp" class="btn btn-xs btn-default" target="_blank"><span class="icon icon-download"></span> Download</a>
                        <button type="button" class="btn btn-xs btn-outline-primary" data-toggle="modal" data-target="#OFPModalCenter">Help</button>
                    </div>
                </div>
            </div>
            @if($client == "bsp")
            <div class="col-md-3">
                <div class="card border-dark">
                    <div class="card-header">
                        <h5 class="card-title">OFP Performance App</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ Storage::get('public/' . $client . '/perf.txt') }}</p>
                        <a href="/download/{{ $client }}/performance" class="btn btn-xs btn-default" target="_blank"><span class="icon icon-download"></span> Download</a>
                        <a href="/download/manual/performance" class="btn btn-xs btn-default"><span class="icon icon-download"></span> Manual</a>
                    </div>
                </div>
            </div>
            @endif
            @if(!($client == "eaa" || $client == "wndd" || $client == "bond" || $client == "has"))
            <div class="col-md-3">
                <div class="card border-dark">
                    <div class="card-header">
                        <h5 class="card-title">Roster Explorer</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{{ Storage::get('public/' . $client . '/re.txt') }}</p>
                        <a href="http://www.offshoreflightplan.co.uk/{{ $client }}/rosterexplorer/publish.htm" class="btn btn-xs btn-default" target="_blank"><span class="icon icon-download"></span> Download</a>
                        <button type="button" class="btn btn-xs btn-outline-primary" data-toggle="modal" data-target="#REModalCenter">Help</button>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-md-3">
                <div class="card border-dark">
                    <div class="card-header">
                        <h5 class="card-title">Documentation</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text">User Manuals</p>
                        <a href="/download/manual" class="btn btn-xs btn-default"><span class="icon icon-download"></span> Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.footer')

<!-- Modal -->
<div class="modal fade" id="OFPModalCenter" tabindex="-1" role="dialog" aria-labelledby="OFPModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="OFPModalLongTitle">To install the latest PC version of Offshore FlightPlan please follow these simple steps.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Download the Offshore FlightPlan installer, offshore_flightplan.msi and save it to your desktop or to your preferred download folder.</li>
                    <li>Click on this to download the full install version of Offshore FlightPlan</li>
                    <li>Double-click on offshore_flightplan.msi to start the automatic installation.</li>
                    <li>Follow any on-screen instructions and accept any pop-up requests to confirm that you wish to proceed.</li>
                    <li>On first running the software, the user password (required for certain admin functions) is set to 'password'. It is recommended that you change this password to something more convenient.</li>
                </ul>
                <p>If you experience any problems when first running Offshore FlightPlan, such as a Runtime Error message or similar, please click <a href="/download/vc">here</a> to download the Microsoft Visual C Redistibrution package. This is normally present on most up-to-date Windows computers and is required by the Oracle database connection software that we use.</p>

                <strong>Network computer users will require administrator rights to install this software. Refer to your system administrator for details.</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="REModalCenter" tabindex="-1" role="dialog" aria-labelledby="REModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="REModalLongTitle">To install the latest PC version of Roster Explorer please follow these simple steps.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Click on the Download button and click on the Install button that will appear (internet connection required). Download and save the file setup.exe to your preferred download location or your desktop.</li>
                    <li>Click on this to download Roster Explorer</li>
                    <li>When download is complete, double-click on the downloaded file, setup.exe, then follow the on-screen instructions, accepting any prompts that may appear.</li>
                </ul>
                <p>
                    <strong>Note:</strong> Updates are released from time to time. When an update is released, you will be advised automatically. Follow the on-screen instructions to update your existing version of Roster Explorer.
                    Whenever you start Roster Explorer, always click Publish/Get roster which can be found in the Roster Actions menu. This will ensure that any entries made by other users are synchronized correctly.
                </p>
                <strong>Network computer users will require administrator rights to install this software.
                    Refer to your system administrator for details.</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/app.js"></script>\
<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>
</body>
</html>


