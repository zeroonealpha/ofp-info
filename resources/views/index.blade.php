<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/x-icon" href="./favicon.ico">

    <title>

        OffshoreFlightPlan

    </title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/application-minimal.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">

    <style>
        /* note: this is a hack for ios iframe for bootstrap themes shopify page */
        /* this chunk of css is not part of the toolkit :) */
        /* …curses ios, etc… */
        @media (max-width: 768px) and (-webkit-min-device-pixel-ratio: 2) {
            body {
                width: 1px;
                min-width: 100%;
                *width: 100%;
            }
            #stage {
                height: 1px;
                overflow: auto;
                min-height: 100vh;
                -webkit-overflow-scrolling: touch;
            }
        }

        .myIcon {
            border-radius: 5px;
            cursor: pointer;
        }

        .featured-list-icon:hover {
            color: #02B8F5;
            border-color: #FB040D;
            animation: 0.2s;
            animation-name: pulse;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }

        /* Modal Content (image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 95%;
        }

        /* Caption of Modal Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            font-size: large;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation */
        .modal-content, #caption {
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
            from {-webkit-transform:scale(0)}
            to {-webkit-transform:scale(1)}
        }

        @keyframes zoom {
            from {transform:scale(0)}
            to {transform:scale(1)}
        }

        /* The Close Button */
        .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
            .modal-content {
                width: 100%;
            }
        }

        .footer-link {
            color: #eee;
        }

        .footer-link:hover {
            color: #629ED6;
        }
    </style>
</head>
<body>

<div class="container pt-4">
    @include('partials.navbar')
</div>
<!-- The Modal -->
<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img id="img01" class="modal-content">
    <div id="caption"></div>
</div>
@if(session()->has('message'))
<div class="block">
    <div class="container">
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading"><span class="icon icon-warning"></span> Forbidden Access Attempt !!</h4>
            <p>{{ session('message') }}</p>
            <hr>
            <p class="mb-0">This action has been logged at {{ \Carbon\Carbon::now('UTC')->toIso8601String() }} from {{ request()->ip() }}</p>
        </div>
        @php session()->forget('message') @endphp
    </div>
</div>
@endif
<div class="block">
    <div class="container text-xs-center">
        <h1 class="block-title mb-0 app-myphone-brand"><img src="/assets/img/Flight%20Software%20Services%20Logo.jpg" style="max-width: 15%">
            Offshore FlightPlan</h1>
        <p class="lead mb-5 pb-5">The Fuel Planning and Performance Solution for Offshore Helicopter Operations.<br><small style="color: darkgrey">Includes Rostering, FTL, Flight Archives & Flight Scheduling</small></p>
        <img src="./assets/img/ofp_box_trans_cropped.png" class="img-fluid" style="max-height: 480px">
    </div>
</div>
<div class="block block-bordered-lg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 text-center mb-5">
                <h1 class="display-2 d-none d-md-block d-lg-none">Tried and Tested</h1>
                <p class="lead mx-auto">Developed since <strong>1998</strong> with extensive client input to produce the best software available.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <ul class="featured-list featured-list-bordered">
                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-list" data-image-path="/assets/img/create_multi_sector_load_plans.png" data-alt="Create Multi Sector Plans"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Create Multi Sector Load Plans</strong></p>
                        <p class="featured-list-icon-text">
                            Produces auto-completed fight log with aircraft performance and fuel plans of up to 30 sectors.
                        </p>
                    </li>

                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-rocket" data-image-path="/assets/img/performance.png" data-alt="Performance"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Performance</strong></p>
                        <p class="featured-list-icon-text">
                            Full aircraft performance covering Class One clear area, offshore helidecks and PC2E.
                        </p>
                    </li>

                </ul>
            </div>
            <div class="col-sm-6">
                <ul class="featured-list featured-list-bordered">

                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-resize-full-screen" data-image-path="/assets/img/helideck_limitations.png" data-alt="Helideck Limitations"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Helideck Limitations</strong></p>
                        <p class="featured-list-icon-text">
                            'D Value' and maximum helideck operating weight's are automatically checked.
                        </p>
                    </li>

                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-users" data-image-path="/assets/img/manage_crew_duty.png" data-alt="Manage Crew Duty, FTL and Roster"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Manage Crew Duty, FTL and Roster</strong></p>
                        <p class="featured-list-icon-text">
                            Inspect Crew hours and availability. Roster months ahead. Organise Checks & Currency Items.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <ul class="featured-list featured-list-bordered">
                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-magnifying-glass" data-image-path="/assets/img/audit_ready_features.png" data-alt="Audit Ready Features"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Audit Ready Features</strong></p>
                        <p class="featured-list-icon-text">
                            Be notified of exceedances such as checks overflown, FTL excursions and FSI notice distribution.
                        </p>
                    </li>

                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-flow-tree" data-image-path="/assets/img/post_flight_analysis.png" data-alt="Post Flight Analysis"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Post Flight Analysis</strong></p>
                        <p class="featured-list-icon-text">
                            All completed flights are available in a standalone reporting website for analysis & billing. Totalize Pax & Freight movements to & from offshore.
                        </p>
                    </li>

                </ul>
            </div>
            <div class="col-sm-6">
                <ul class="featured-list featured-list-bordered">

                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-paper-plane" data-image-path="/assets/img/whiteboard.png" data-alt="Flight Scheduling"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Flight Scheduling</strong></p>
                        <p class="featured-list-icon-text">
                            View planned flights on a big-screen departures board, in list or bar format.
                        </p>
                    </li>

                    <li class="mb-5">
                        <div class="featured-list-icon text-primary">
                            <span class="myIcon icon icon-open-book" data-image-path="/assets/img/pilot_logbook.png" data-alt="Pilot Logbook"></span>
                        </div>
                        <p class="featured-list-icon-text mb-0"><strong>Pilot Logbook</strong></p>
                        <p class="featured-list-icon-text">
                            Pilots have access to Logbook, Roster and Crew-to-Read notices via specialist web-portal.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="block block-bordered-lg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4 text-justify mb-5">
                <p>
                <H2><span class="icon icon-arrow-right"></span> Offshore FlightPlan first came about in 1998</H2> when developer and offshore helicopter pilot, Graham Pettican, noticed that his employer at that time had no satisfactory method of flight planning for their North Sea helicopter operations. He set about creating a software solution that would allow pilots to easily work out their fuel requirements. At first, the software printed out basic fuel tables based on the routes to be flown. It was soon adopted by the original Bond Helicopters then taken up by CHC UK who planned many thousands of flights with it. The software was found to be particularly useful for multi-sector flights involving numerous stops including offshore refuelling.
                Since those early days, the UK specialist software company Flight Software Services Ltd. was set up and the software now handles all aspects of offshore helicopter operational requirements including flight planning, aircraft performance such as PC1, PC2E, PC2DLE and CAT A. Several helicopter types including the AW139 and AW189 are catered for and the software also takes care of crew duty recording and monitoring, crew training checks, rostering and flight data analysis which can be used for invoicing and auditing.
                </p>
            </div>
            <div class="col-md-4 text-justify mb-5">
                <p>“We have found that, over the years, most of the major oil companies and regional authorities have carried out numerous satisfactory audits involving our software. Our customers include CHC, Weststar, Brunei Shell Petroleum, Hevilift, NHV and Heliconia operating in countries all around the world. Not only that but most of the major oil companies including Shell, BP, Hess, Petrofac, Perenco, Exxon Mobil, Repsol and Petronas have carried audits involving our software.” said CEO and developer Graham Pettican. “Our software has developed beyond all recognition especially since Craig Webster joined the project. One of our biggest issues was how to get computers from all around the world to talk to each other when many countries still suffer with slow or intermittent internet connections. We resolved that by incorporating some very clever synchronisation software which works even under the most extreme conditions. We have servers scattered all around the world and they are all in sync with each other so everyone gets the latest data, wherever they are.”
                    Of great interest to many operators is the ability to operate at offshore locations without exposing the aircraft to possible ditching in the event of a single engine failure. This requires the crews to consult several graphs for each sector to find out what the restricted take-off weight (RTOM) and restricted landing weight (RLM) are. Offshore FlightPlan takes care of all this with one simple weather entry and makes sure available payloads are offered which ensure full compliance at all times, both on take-off and landing.
                </p>
            </div>
            <div class="col-md-4 text-justify mb-5">
                <p>Over 2000 flights each month are handled with Offshore FlightPlan whilst over 80,000 crew duties have so far been recorded and checked for legality. “Our included roster software works together with Offshore FlightPlan to form an integrated system. When crew rosters are created, the system checks for legality based on future planned duties together with duties already carried out to ensure FTL compliance.” said Craig.
                    The cost of this software is significantly lower than their competitors because the license fee is based on the number of aircraft, not the number of computers. This makes the package attractive to operators with one or two aircraft whilst operators with larger fleets benefit from quantity discounts. They are now working on a paperless solution using Windows tablets. “We chose the Windows platform because there are more models to choose from, including ‘tough’ models. Also, we can use removable memory cards to store the data on. This is useful should a tablet become unserviceable during the flight as the user simply takes the memory card out and places it in the spare tablet. That cannot be done with an iPad.”
                </p>
            </div>
        </div>
    </div>
</div>
<div class="block block-bordered-lg pb-0 app-block-stats">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-4">
                <div data-grid="images" id="mygrid">
                    <img data-width="191" data-height="191" src="./assets/img/nhv.jpg">
                    <img data-width="80" data-height="25" src="./assets/img/hevilift.png">
                    <img data-width="250" data-height="83" src="./assets/img/bsp.png">
                    <img data-width="681" data-height="272" src="./assets/img/weststar.png">
                    <img data-width="249" data-height="48" src="./assets/img/heliconia.png">
                </div>
            </div>
            <div class="col-md-5 text-center text-md-left">
                <p class="lead">
                    Since 1998 we have planned thousands of flight for clients including Weststar Sdn Bhd, Brunei Shell Petroleum, CHC, NHV and Heliconia. Accepted by auditors from such Oil & Gas operators as Shell, Hess, Repsol, Petronas, BP, ExxonMobil, Conoco Philips, Tullow to name but a few.
                </p>
                <div class="row mb-3 d-none d-lg-flex">
                    <div class="col-6 mb-3 text-xs-center">
                        <div class="statcard">
                            <h2 class="statcard-number block-title">78</h2>
                            <span class="statcard-desc">Helicopters</span>
                        </div>
                    </div>
                    <div class="col-6 mb-3 text-xs-center">
                        <div class="statcard">
                            <h2 class="statcard-number block-title">388</h2>
                            <span class="statcard-desc">Pilots</span>
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-6 mb-3">
                        <div class="statcard text-xs-center">
                            <h2 class="statcard-number block-title">{{  DB::connection('weststar')->table('flights_done_general')->count() }}</h2>
                            <span class="statcard-desc">Flights Planned To Date</span>
                        </div>
                    </div>
                    <div class="col-6 mb-3 text-xs-center">
                        <div class="statcard">
                            <h2 class="statcard-number block-title">{{  DB::connection('weststar')->table('crewduty')->count() }}</h2>
                            <span class="statcard-desc">Duties Logged To Date</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block block-bordered-lg block-overflow-hidden app-block-design">
    <div class="container">
        <div class="row app-pos-r justify-content-between">
            <div class="col-sm-6 text-sm-left">
                <p class="lead text-justify">
                    <span class="icon icon-light-bulb"></span>
                    <strong>Ready to dig deeper?</strong><br>
                    Firstly, check out our video which demonstrates just how easy it is to create a complex flight plan, including performance limitations and fuel calculations. Have a look at
                    the user manual to explore some of the more complex features. Or, <a href="/contact">Contact Us</a> to arrange a free trial, a site visit or just to talk things through. We'd be delighted
                    to hear from you.
                </p>
                <hr>
                <div class="row">
                    <div class="col text-center">
                        <div class="statcard">
                            <p class="statcard-number block-title">Look at the user manual</p>
                            <span class="statcard-desc"><span class="icon icon-download"></span>&nbsp<a href="/download/manual">Click Here</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/itNd2f11iHc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block block-bordered-lg text-center">
    <div class="container-fluid col-sm-6">
        <p class="lead mb-4" style="color: #666">
            <span class="icon icon-chat"></span>
            If you would like a free trial will provide you with a tailored version for your operation which you can try out for a few weeks without any obligation.
        </p>
        <form class="form-inline d-flex justify-content-center">
            <a class="btn btn-primary mb-1 ml-sm-1 animated-button" href="/licensing">Contact Us for a Free Trial</a>
        </form>
        <small class="text-muted">
            (You will be required to provide some contact details)
        </small>
    </div>
</div>
@include('partials.footer')
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/app.js"></script>
<script>
    $(document).ready(function () {
        $('#mygrid').imageGrid();

        // Get the modal
        $('.myIcon').click(function(event) {
            $('#img01').attr('src', this.attributes['data-image-path'].value);
            $('#caption').html(this.attributes['data-alt'].value);
            $('#myModal').show();
       });

        //When the user clicks on <span> (x), close the modal
        $('.close').click(function() {
            $('#myModal').hide();
        });
    });
</script>
</body>
</html>

