<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="./favicon.png">

    <title>

        OffshoreFlightPlan

    </title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/application-minimal.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <style>
        /* note: this is a hack for ios iframe for bootstrap themes shopify page */
        /* this chunk of css is not part of the toolkit :) */
        /* …curses ios, etc… */
        @media (max-width: 768px) and (-webkit-min-device-pixel-ratio: 2) {
            body {
                width: 1px;
                min-width: 100%;
                *width: 100%;
            }
            #stage {
                height: 1px;
                overflow: auto;
                min-height: 100vh;
                -webkit-overflow-scrolling: touch;
            }
        }

        .footer-link {
            color: #eee;
        }

        .footer-link:hover {
            color: #629ED6;
        }


    </style>
</head>
<body>

<div class="container pt-4">
    @include('partials.navbar')
</div>
@if (isset($message))
<div class="container mb-5">
    <h2 class="mt-0">
        <strong class="text-ribbon text-ribbon-info">
            <span><span class="icon icon-thumbs-up"></span> {{ $message }}</span>
        </strong>
    </h2>
</div>
@endif
<div class="container">
    <h2 class="mt-0">
        <strong class="text-ribbon text-ribbon-info">
            <span><span class="icon icon-arrow-right"></span> Licensing an Offshore FlightPlan product</span>
        </strong>
    </h2>
    <p class="mt-4">
      <span class="text-ribbon text-ribbon-info text-white">
        <span>To obtain further information about pricing and how to take advantage of our free trial period, please provide your details below</span>
      </span>
    </p>
    <br>
    @include('partials.errors')
    <div class="container-content-middle mb-5">
        <form class="m-auto" method="POST" action="/licensing/enquiry">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputName">Name *</label>
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="name">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputCompany">Company *</label>
                    <input type="text" class="form-control" id="inputCompany" placeholder="My Company" name="company">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-8">
                    <label for="inputEmail">Email *</label>
                    <input type="email" class="form-control" id="inputEmail" placeholder="email@domain.com" name="email">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputTel">Telephone *</label>
                    <input type="text" class="form-control" id="inputTel" placeholder="01234 567 890" name="telephone">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputCity">City *</label>
                    <input type="text" class="form-control" id="inputCity" placeholder="City" name="city">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputCountry">Country *</label>
                    <input type="text" class="form-control" id="inputCountry" name="country">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputAircraftNum">Number Of Aircraft *</label>
                    <select id="inputAircraftNum" class="form-control" name="aircraft">
                        <option selected>Choose...</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                        <option>More than 10</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputBaseNum">Number Of Bases *</label>
                    <select id="inputBaseNum" class="form-control" name="bases">
                        <option selected>Choose...</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>More than 5</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputBaseNum">Interested In?</label>
                    <select id="inputBaseNum" class="form-control" name="interest">
                        <option selected>Choose...</option>
                        <option value="Flight Planning">Flight Planning</option>
                        <option value="Rostering">Rostering</option>
                        <option value="Flight Planning & Rostering">Both</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleFormControlQuestions">Questions?</label>
                <textarea class="form-control" id="exampleFormControlQuestions" rows="3" name="questions">Ask here ...</textarea>
            </div>
            <div class="form-group">
                <label for="exampleFormControlQuestions"><b>Prove you are a not a robot. Please tell us how many blades an AW139 has?</b></label>
                <select class="form-control" name="robot" id="robot">
                    <option value="0">None, it's magical.</option>
                    <option value="1">Just one.</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                    <option value="4">Four</option>
                    <option value="5">Five</option>
                    <option value="6">Six</option>
                    <option value="7">Seven</option>
                </select>
            </div>
            <div class="text-md-right">
                <small class="text-md-right"> * is a required field</small>
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </form>
    </div>
</div>
@include('partials.footer')

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>






