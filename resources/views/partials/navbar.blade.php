<nav class="navbar navbar-light navbar-expand-sm text-uppercase app-navbar mb-5">
    <a class="navbar-brand mr-auto d-none d-sm-block" href="../">
        <span><img src="/assets/img/Flight%20Software%20Services%20Logo.jpg" style="max-width: 50px">&nbsp;Flight Software Services Ltd</span>
    </a>
    <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item ">
                <a class="nav-link" href="/"><span class="icon icon-home"></span> @auth [{{ auth()->user()->name }}] @endauth</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Client Area</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/client-area/bond/">Bond Helicopters</a>
                    <a class="dropdown-item" href="/client-area/bsp/">Brunei Shell Petroleum</a>
                    <a class="dropdown-item" href="/client-area/eaa/">Euro Asia Air</a>
                    <a class="dropdown-item" href="/client-area/has/">HAS International</a>
                    <a class="dropdown-item" href="/client-area/nhv/">NHV</a>
                    <a class="dropdown-item" href="/client-area/phieu/">PHI EU</a>
                    <a class="dropdown-item" href="/client-area/westair/">Westair</a>
                    <a class="dropdown-item" href="/client-area/weststar/">Weststar Sdn Bhd</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Contact</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="/contact/">Contact Us</a>
                    <a class="dropdown-item" href="/licensing/">Software Licensing Enquiries</a>
                    <a class="dropdown-item" href="/newsletter/">Newsletter</a>
                </div>
            </li>
        </ul>
    </div><!--/.nav-collapse -->
</nav>
