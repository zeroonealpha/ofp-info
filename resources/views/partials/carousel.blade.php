<div class="block block-bordered-lg pl-0 pt-0 pr-0">
    <div id="carousel-example-generic" class="carousel carousel-light slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <div class="block">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-sm-8 text-center">
                                <img class="d-block img-fluid" src="/assets/img/heli8.jpg"><br>
                                <h2 class="mt-0">
                                    <strong class="text-ribbon text-ribbon-primary">
                                        <span style="color: white">Up to-date</span>
                                    </strong>
                                </h2>
                                <p class="mt-4">
                                  <span class="text-ribbon text-ribbon-primary">
                                    <span style="color: white">Performance included for all modern Offshore types, including the AW139 and AW189.</span>
                                  </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="block">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-sm-8 text-center">
                                <img class="d-block img-fluid" src="/assets/img/heli5.jpg"><br>
                                <h2 class="mt-0">
                                    <strong class="text-ribbon text-ribbon-info">
                                        <span style="color: white">Specially Designed for Offshore Operations</span>
                                    </strong>
                                </h2>
                                <p class="mt-4">
                                  <span class="text-ribbon text-ribbon-info">
                                    <span style="color: white">Client driven input from Offshore Operators for over 20 years</span>
                                  </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="block">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-sm-8 text-center">
                                <img class="d-block img-fluid" src="/assets/img/audit.png"><br>
                                <h2 class="mt-0">
                                    <strong class="text-ribbon text-ribbon-success">
                                        <span style="color: white">Audit Ready</span>
                                    </strong>
                                </h2>
                                <p class="mt-4">
                                  <span class="text-ribbon text-ribbon-success">
                                    <span style="color: white">Breeze through audits with backend features.</span>
                                  </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="icon icon-chevron-thin-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="icon icon-chevron-thin-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


