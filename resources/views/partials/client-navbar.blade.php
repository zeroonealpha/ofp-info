<nav class="navbar navbar-light navbar-expand-sm text-uppercase app-navbar mb-5">
    <a class="navbar-brand mr-auto" href="../">
        <span><img src="/assets/img/Flight%20Software%20Services%20Logo.jpg" style="max-width: 50px">&nbsp;Flight Software Services Ltd</span>
    </a>
    <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Contact</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="/contact/">Contact Us</a>
                    <a class="dropdown-item" href="/licensing/">Software Licensing Enquiries</a>
                    <a class="dropdown-item" href="/newsletter/">Newsletter</a>
                </div>
            </li>
            <li class="nav-item ">
                <form class="" action="/logout" method="post">
                    {{ csrf_field() }}
                    <button class="nav-link" type="submit"><span class="icon icon-log-out"></span>Logout</button>
                </form>
            </li>
        </ul>
    </div><!--/.nav-collapse -->
</nav>

