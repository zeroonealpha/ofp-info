1<div class="block app-block-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mb-4">
                <ul class="list-unstyled list-spaced">
                    <li><h6 class="text-uppercase">&copy; 1998-{{ \Carbon\Carbon::now()->year }} Flight Software Services</h6></li>
                    <li><h6>A Registered Company in England & Wales. </h6></li>
                    <li><h6>Telephone +44 1603 441053</h6></li>
                    <li><h6>Company Number 08500398</h6></li>
                </ul>
            </div>
            <div class="col-sm-4 mb-4">
                <ul class="list-unstyled list-spaced h6">
                    <li><h6 class="text-uppercase">OTHER PRODUCTS</h6></li>
                    <li><a class="footer-link" href="https://www.helicrewmanager.com">HeliCrewManager</a></li>
                </ul>
            </div>
            <div class="col-sm-2 mb-4">
                <ul class="list-unstyled list-spaced h6">
                    <li><h6 class="text-uppercase">LINKS</h6></li>
                    <li><a href="/contact" class="footer-link">Online Support Form</a></li>
                    <li><a href="/licensing" class="footer-link">Licensing Enquiries</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


