let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('resources/assets/dist/toolkit-minimal.css', 'public/css/app.css');
mix.copy('resources/assets/dist/toolkit.js', 'public/js/app.js');

mix.copyDirectory('resources/assets/docs/assets/img', 'public/assets/img');
mix.copyDirectory('resources/assets/docs/assets/fonts', 'public/assets/fonts');
